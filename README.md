# wAVdio

wAVdio, the wireless audio video guide, is an open source, multi media guide
for museums. It is implemented as a mobile first web app and can thus be run
on a usual smartphone without installation. Visitors can either browse the
musuems exhibits or scan an exhibit's QR code to find a specific exhibit's
information directly. In addition to text, each exhibit can be presented using
images, audio and video. Currently, the app is available in English, German, 
French and Spanish.

A dedicated content management system, the admin interface, allows the admin
to manage the content presented in the visitor interface. Beyond that, it
supports the generation of the exhibits' QR codes and provides the museum
feedback in form of comments and like statistics. Further functionality
includes managing informational pages and legal data.

Screenshots and further details can be found in the wiki:
https://gitlab.com/wavdio/wiki/-/wikis/home.

# Technical Architecture

At the top level, the app consists of the frontend, the backend and the
database. The frontend is implemented as an SPA (Single Page Application),
i.e. the app is completely loaded into the browser which improves performance
once it is loaded the first time.

<!-- Angular -->
The frontend is implement using Google's Angular framework
(https://angular.io/). Typical characteristics of the Angular framework include
its modularity, the usage of TypeScript and the fact that it produces SPAs
by default.

<!-- Node, Express  -->
The backend is implemented in JavaScript that is interpreted by Node.js 
(https://nodejs.org/). The npm package manager (https://www.npmjs.com/) is 
used for dependency management. The most important dependency is the Express.js
library (https://expressjs.com/) that is responsible for establishing the web
server.

<!-- MongoDB -->
As database MongoDB (https://www.mongodb.com/) is used. It is a NoSQL database
that stores the information in form of documents instead of rows in tables.

# Setup

To quickly get a development environment running, follow these steps:

1. Install Node.js, npm and MongoDB
2. Ensure that MongoDB is running
3. Start the backend
   1. Clone the `wavdio-express` repo from https://gitlab.com/wavdio/wavdio-express
   2. Run `npm install`
   3. Run `npm start`
4. Start the frontend
   1. Clone the `wavdio-angular` repo from https://gitlab.com/wavdio/wavdio-angular
   2. Run `npm install`
   3. Run `npm start`
5. Open wAVdio at http://localhost:4200/

For production deployment, at least the Angular frontend should be packed and
deployed to a suitable web server like Apache or nginx.

For development, the graphical MongoDB tool Compass 
(https://www.mongodb.com/products/compass) is very handy.